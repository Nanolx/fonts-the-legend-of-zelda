all:
	@echo "nothing to do"

clean:
	@echo "nothing to clean"

install:
	mkdir -p $(DESTDIR)/usr/share/fonts
	cp -vr opentype/ $(DESTDIR)/usr/share/fonts/
	cp -vr truetype/ $(DESTDIR)/usr/share/fonts/
