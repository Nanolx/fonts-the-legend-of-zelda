# The Legend of Zelda Fonts

Official The Legend of Zelda game fonts or fonts  inspired by the popular Nintendo gaming series.

## License

All fonts provided as-is for non-commercial use

  (c) 1986 - 2017 Nintendo

additional credits to the creators of the remade fonts.

## Installation

### GNU/Linux / Other Unixoid

	`sudo make install`

### Debian GNU/Linux

	`dpkg-buildpackage -rfakeroot`

install the created .deb-package using

	`sudo dpkg -i <package>`

### OtherOS

Dunno.

## OpenType Fonts

   * Ancient Hylian (Wind Waker)
   * Charlemagne Bold (Logo)
   * Hylia Serif (Remake/Logo)
   * Hylian Bold (TP)
   * Hylian Regular (TP)

## TrueType Fonts

   * Triforce (Logo)
   * Hylian (Skyward Sword)
   * Charlemagne Regular (Logo)
   * Hylian Sarif (Remake/OT)
   * Hylian (OT)
   * Ravenna (Logo)
   * Amercian Uncial (Logo)
   * DX BBK (LA)
   * Return of Ganon (ALTTP)
   * Sherwood (WW)
   * Gerudo Typography (OT)
   * Hylian (Symbols)
   * Hylian Sarif (Remake/Symbols)
   * Hylian Rounded (Remake/Symbols)
   * Gerudo Sarif (Remake/OT)
   * Goron Sarif (Remake/OT)
   * Zoran Sarif (Remake/OT)
   * Text (OA/OS)
